"""Terrains for different robot classes."""
from .mujoco_terrain import TerrainEnv
from gym.envs.mujoco.half_cheetah_v3 import HalfCheetahEnv
from gym import utils


class HalfCheetahTerrainEnv(TerrainEnv, HalfCheetahEnv):
    def __init__(
                self,
                xml_file="half_cheetah.xml",
                forward_reward_weight=1.0,
                ctrl_cost_weight=0.1,
                reset_noise_scale=0.1,
                exclude_current_positions_from_observation=True,
                terrain_x_scale: float = 0.9,
                terrain_y_scale: float = 0.5,
                terrain_z_scale: float = 2.0,
                platform_height: float = 0.5,
                platform_width: float = 0.2,
                task=None,
            ):
        utils.EzPickle.__init__(**locals())
        self._forward_reward_weight = forward_reward_weight
        self._ctrl_cost_weight = ctrl_cost_weight
        self._reset_noise_scale = reset_noise_scale
        self._exclude_current_positions_from_observation = exclude_current_positions_from_observation
        TerrainEnv.__init__(
                self,
                xml_file,
                1,
                task=task,
                x_scale=terrain_x_scale,
                y_scale=terrain_y_scale,
                z_scale=terrain_z_scale,
                platform_height=platform_height,
                platform_width=platform_width,
            )

    def step(self, *args, **kwargs):
        observation, reward, done, info = super().step(*args, **kwargs)
        done = done or self.is_out_of_bounds
        return observation, reward, done, info
