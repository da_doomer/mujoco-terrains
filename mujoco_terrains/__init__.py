from .envs import HalfCheetahTerrainEnv
from .random_terrains import random_terrain
import gym

gym.envs.register(
    id="HalfCheetahTerrainEnv-v0",
    entry_point="mujoco_terrains:HalfCheetahTerrainEnv",
)
