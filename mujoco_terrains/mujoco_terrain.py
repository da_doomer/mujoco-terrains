"""Parametric terrains for Mujoco-based environments."""
from pathlib import Path
from typing import List
from typing import Tuple
from os import path
import xml.etree.ElementTree as ET
import gym.envs.mujoco as mjcf_module
from gym.envs.mujoco.mujoco_env import MujocoEnv
import mujoco_py
import numpy as np


material = ET.fromstring(
        """<material name="MatCube_MUJOCO_TERRAIN" reflectance="0.5" shininess="1" specular="1" texrepeat="3 3" texture="texplane"/>"""
    )

def x_axis_platform(x1, z1, x2, z2, width, height) -> ET:
    """Return the XML sub-tree corresponding to the given cube"""
    # conaffinity is set using half_cheetah.xml as example
    # more information:
    # http://mujoco.org/book/XMLreference.html
    pos = ((x1+x2)/2, 0.0, (z1+z2)/2)
    length = np.linalg.norm(np.array([x1, z1])-np.array([x2, z2]))
    size = (length, width/2, height)
    origin = np.array([x1, z1])
    u = np.array([x2, z1])-origin
    v = np.array([x2, z2])-origin
    angle = np.math.atan2(np.linalg.det([u, v]), np.dot(u, v))/2
    euler = (0.0, -angle, 0.0)
    platform = f"""<body pos="{" ".join(map(str, pos))}">
         <geom type="box" conaffinity="1" euler="{" ".join(map(str, euler))}" size="{" ".join(map(str, size))}" rgba="0.8 .7 0.8 1" material="MatCube_MUJOCO_TERRAIN"/>
      </body>"""
    return ET.fromstring(platform)


def add_terrain(
        model_path: Path,
        terrain: List[float],
        x_scale: float,
        y_scale: float,
        z_scale: float,
        platform_height: float,
        platform_width: float,
        ) -> str:
    """Add the given terrain to the xml in the given path, and return an XML
    string."""
    tree = ET.parse(model_path)

    # Add the new texture
    asset = tree.getroot().find("asset")
    if asset is None:
        tree.getroot().append(ET.fromstring("<asset></asset>"))
        asset = tree.getroot().find("asset")
    asset.append(material)

    # Remove the old floor, if any
    worldbody = tree.getroot().find("worldbody")
    for geom in worldbody.findall("geom"):
        try:
            if geom.attrib["name"] == "floor":
                worldbody.remove(geom)
        except AttributeError:
            pass

    # Add each platform
    for i in range(len((terrain))-1):
        x1 = (i-1)*x_scale-x_scale/2.0
        x2 = (i-1)*x_scale
        z1 = terrain[i]*z_scale-platform_height
        z2 = terrain[i+1]*z_scale-platform_height
        platform = x_axis_platform(x1, z1, x2, z2, y_scale, platform_width)
        worldbody.append(platform)
    return ET.tostring(tree.getroot(), encoding="unicode")


class TerrainEnv(MujocoEnv):
    """Super class for all terrain environments. This class must be subclassed
    inheriting from Mujoco environment classes."""
    def __init__(
                self,
                model_path,
                frame_skip,
                task: List[float],
                x_scale: float,
                y_scale: float,
                z_scale: float,
                platform_height: float,
                platform_width: float,
                ):
        if model_path.startswith("/"):
            fullpath = model_path
        else:
            fullpath = Path(mjcf_module.__file__).parent/"assets"/model_path
        if not path.exists(fullpath):
            raise IOError("File %s does not exist" % fullpath)
        self.frame_skip = frame_skip
        self.task = task
        self.x_scale = x_scale
        self.y_scale = y_scale
        self.z_scale = z_scale
        model = add_terrain(
                fullpath,
                task,
                x_scale,
                y_scale,
                z_scale,
                platform_height,
                platform_width,
            )
        self.model = mujoco_py.load_model_from_xml(model)
        self.sim = mujoco_py.MjSim(self.model)
        self.data = self.sim.data
        self.viewer = None
        self._viewers = {}
        self.metadata = {
            "render.modes": ["human", "rgb_array", "depth_array"],
            "video.frames_per_second": int(np.round(1.0 / self.dt)),
        }
        self.init_qpos = self.sim.data.qpos.ravel().copy()
        self.init_qvel = self.sim.data.qvel.ravel().copy()
        self._set_action_space()
        action = self.action_space.sample()
        observation, _reward, done, _info = self.step(action)
        assert not done
        self._set_observation_space(observation)
        self.seed()

    @property
    def is_out_of_bounds(self) -> bool:
        """True if the robot is outside the bounds of the terrain."""
        x_pos = self.sim.data.qpos[0]
        min_x = -self.x_scale
        max_x = (1+len(self.task))*self.x_scale
        if x_pos < min_x:
            return True
        if x_pos > max_x:
            return True
        return False
