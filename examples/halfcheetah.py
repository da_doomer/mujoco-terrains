import random
from functools import reduce
from typing import List
import gym
import mujoco_terrains


def random_terrain(terrain_len: int, section_len) -> List[float]:
    def slope_creator(slope):
        return (0,) + tuple(slope*i for i in range(terrain_len))

    def extended_terrain(terrain, section):
        new_terrain = list(terrain)
        for height in section:
            new_terrain.append(terrain[-1] + height)
        return tuple(new_terrain)

    def random_combined_terrain(sections, terrain_len):
        terrain = tuple()
        section = _random.choice(sections)
        while len(terrain) < terrain_len:
            if len(terrain) == 0:
                terrain = section[:section_len]
            else:
                terrain = extended_terrain(terrain, section[:section_len])
            next_section = _random.choice(sections)
            while next_section == section and len(sections) > 1:
                next_section = _random.choice(sections)
            section = next_section
        return terrain

    flat = slope_creator(0.0)
    up = slope_creator(0.2)
    down = slope_creator(-0.2)
    hurdles = ((0, 0, 0, 0, 0, 0, 0, 0.2, 0.2)*terrain_len)[:terrain_len]
    step = (0,)*4 + (0.2,)*4
    stairs = reduce(
            extended_terrain,
            [step for _ in range(terrain_len)],
        )[:terrain_len]
    gaps = (((0,)*4 + (0.1,)*4)*terrain_len)[:terrain_len]
    _random = random.Random("generate some cool tasks!")

    # Build tasks
    terrains = [flat, up, hurdles, stairs, gaps, down]
    return random_combined_terrain(terrains, terrain_len)


section_len = 10
terrain = random_terrain(100, section_len)
env = gym.make("HalfCheetahTerrainEnv-v0", task=terrain)
observation = env.reset()
for _ in range(1000):
    env.render()
    action = env.action_space.sample()
    observation, reward, done, info = env.step(action)
    if done:
        observation = env.reset()
env.close()
