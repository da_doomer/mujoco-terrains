# Mujoco Terrains

**These environments depend on the closed-source Mujoco simulator being
installed. Users should prefer
[pybullet-terrains](https://gitlab.com/da_doomer/pybullet-terrains), which run
on open-source PyBullet.**

Simple parametric terrains for Mujoco-based environments.

## Installation

Use your favorite package manager. With pip this is:

```bash
python -m pip install git+https://gitlab.com/da_doomer/mujoco-terrains.git
```

## Use

See the examples.
