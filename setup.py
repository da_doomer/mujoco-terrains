#!/usr/bin/env python3
from setuptools import setup

setup(
    name='mujoco_terrains',
    version='0.1',
    install_requires=[
        'gym',
        'mujoco_py',
    ],
    description='Parametric terrains for Mujoco-based environmets.',
    packages=['mujoco_terrains'],
    author='da_doomer',
    license='MIT',
    platforms='Linux; Windows; OS X'
    )
